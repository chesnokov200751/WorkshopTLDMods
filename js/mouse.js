const cursor1 = document.querySelector('.cursor-1');
const cursor2 = document.querySelector('.cursor-2');
const hoverables = document.querySelectorAll('.hoverable');

// Register listeners.
document.body.addEventListener('mousemove', onMouseMove);
for (let i = 0; i < hoverables.length; i++) {
  hoverables[i].addEventListener('mouseenter', onMouseHover);
  hoverables[i].addEventListener('mouseleave', onMouseHoverOut);
}

// Move the cursor.
function onMouseMove(e) {
  TweenMax.to(cursor2, .4, {
    x: e.pageX - 5,
    y: e.pageY - 7
  })
  TweenMax.to(cursor1, .1, {
    x: e.pageX - 5,
    y: e.pageY - 7
  })
}

// Hover an element
function onMouseHover() {
  $('.cursor-1, .cursor-2').addClass('hover');
}
function onMouseHoverOut() {
  $('.cursor-1, .cursor-2').removeClass('hover');
}